﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Moverobjeto : MonoBehaviour
{
    public float Horizontal;
    public  float vertical;
    public CharacterController GameObject;
    public float velocidad ;
    public bool salto;
    public int aumento = 2;
    public int limite = 0;
    public int recuperado = 0;
        // Start is called before the first frame update
    void Start()
    {
        GameObject = GetComponent<CharacterController>();
        
    }
    
    // Update is called once per frame
    void Update()
    {
        Horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        salto = Input.GetKey(KeyCode.LeftShift);
        GameObject.Move(new Vector3(Horizontal, 0, vertical) * velocidad * Time.deltaTime);
    }
       private void FixedUpdate()
    {
      
        if (salto == true)
        {
            limite++;
            
            if(limite <300)
            {
                GameObject.Move(new Vector3(Horizontal, 0, vertical) * velocidad * Time.deltaTime * aumento);
                
            }
           
        }
        if (limite > 300)
        {
            recuperado++;
            if (recuperado >= 600)
            {
                limite = 0;
                recuperado = 0;
            }
        }

    }
}
